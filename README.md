# NLP_NLTK_exploration

Exploring basic NLTK functionality:
 - Finding similar words in the text
 - Counting and word distributions
 - Collocations
 - Finding specific items
 - Exploring stylistic differences between different genre texts
 - Semantics using WordNet